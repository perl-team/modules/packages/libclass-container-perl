libclass-container-perl (0.13-2) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from deprecated 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 11 Jun 2022 22:24:50 +0100

libclass-container-perl (0.13-1) unstable; urgency=medium

  * Team upload

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * Remove Fabrizio Regalli from Uploaders. Thanks for your work!

  [ Damyan Ivanov ]
  * New upstream version 0.13
  * bump debhelper compatibility level to 9
  * declare conformance with Policy 4.1.1 (no changes needed)

 -- Damyan Ivanov <dmn@debian.org>  Sat, 04 Nov 2017 17:49:07 +0000

libclass-container-perl (0.12-4) unstable; urgency=low

  * Team upload.

  [ Ansgar Burchardt ]
  * Email change: Ansgar Burchardt -> ansgar@debian.org
  * debian/control: Convert Vcs-* fields to Git.

  [ Fabrizio Regalli ]
  * Bump to 3.9.2 Standard-Version.
  * Add myself to Uploaders and Copyright.
  * Switch d/compat to 8.
  * Build-Depends: switch to debhelper (>= 8).
  * Fixed lintian copyright-refers-to-symlink-license message.
  * Moved d/rules to standard 3 lines.

  [ gregor herrmann ]
  * Remove alternative (build) dependencies that are already satisfied
    in oldstable.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.
  * Add explicit build dependency on libmodule-build-perl.
  * debian/copyright: switch formatting to Copyright-Format 1.0.

 -- gregor herrmann <gregoa@debian.org>  Mon, 08 Jun 2015 21:41:28 +0200

libclass-container-perl (0.12-3) unstable; urgency=low

  [ Ansgar Burchardt ]
  * Now maintained by the Debian Perl Group. (Closes: #569472)
    + Add myself to Uploaders.
  * debian/control: Add Vcs-*, Homepage fields.
  * Make build-dep on perl unversioned.
  * No longer suggest perl (>= 5.8.2) | libscalar-util-perl: Debian's perl is
    recent enough.
  * debian/control: Minor changes to description.
  * Convert debian/copyright to proposed machine-readable format.
  * Use source format 3.0 (quilt).
  * Use debhelper 7 instead of CDBS.
  * No longer install README: content identical to module documentation.
  * debian/watch: Use extended regular expression to match upstream releases.
  * Remove Emacs mode settings from debian/changelog.
  * Bump Standards-Version to 3.8.4.

  [ gregor herrmann ]
  * Add build dependency on Module::Build; remove version from (build)
    dependency on libparams-validate-perl.

 -- Ansgar Burchardt <ansgar@43-1.org>  Wed, 19 May 2010 21:53:29 +0900

libclass-container-perl (0.12-2) unstable; urgency=low

  * Moved to CDBS
  * Update to standards version 3.7.2
  * New maintainer email address

 -- Charles Fry <cfry@debian.org>  Wed,  5 Jul 2006 12:54:37 -0400

libclass-container-perl (0.12-1) unstable; urgency=low

  * New maintainer. (Closes: #329626)
  * New upstream release. (Closes: #329510)

 -- Charles Fry <debian@frogcircus.org>  Thu, 22 Sep 2005 10:01:26 -0400

libclass-container-perl (0.11-0.1) unstable; urgency=low

  * Non-maintainer upload to fix an upstream bug in the last NMU.
  * New upstream release

 -- Michael Alan Dorman <mdorman@debian.org>  Thu,  4 Mar 2004 08:30:17 -0500

libclass-container-perl (0.10-0.1) unstable; urgency=low

  * Non-maintainer upload with maintainer agreement
  * New upstream release (closes: bug#229082)

 -- Michael Alan Dorman <mdorman@debian.org>  Tue, 10 Feb 2004 12:41:24 -0500

libclass-container-perl (0.07-1) unstable; urgency=low

  * New upstream release (closes: Bug#156459)

 -- Ivan Kohler <ivan-debian@420.am>  Mon, 12 Aug 2002 12:25:55 -0700

libclass-container-perl (0.05-1) unstable; urgency=low

  * Initial Release (closes: Bug#152856).

 -- Ivan Kohler <ivan-debian@420.am>  Sat, 13 Jul 2002 07:34:34 -0700

Source: libclass-container-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Ansgar Burchardt <ansgar@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libmodule-build-perl
Build-Depends-Indep: perl,
                     libparams-validate-perl
Standards-Version: 4.1.3
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libclass-container-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libclass-container-perl.git
Homepage: https://metacpan.org/release/Class-Container

Package: libclass-container-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libparams-validate-perl
Description: Perl module to glue object frameworks together transparently
 The Class::Container module facilitates building frameworks of several classes
 that inter-operate. It was first designed and built for HTML::Mason, in which
 the Compiler, Lexer, Interpreter, Resolver, Component, Buffer, and several
 other objects must create each other transparently, passing the appropriate
 parameters to the right class, possibly substituting their own subclass for any
 of these objects.
 .
 The main features of Class::Container are:
 .
  * Declaration of parameters used by each member in a class framework
 .
  * Transparent passing of constructor parameters to the class that
    needs them
 .
  * Ability to create one (automatic) or many (manual) contained
    objects automatically and transparently
